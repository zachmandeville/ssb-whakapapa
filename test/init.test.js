const test = require('tape')
const get = require('lodash.get')

const Server = require('./test-bot')
const { manifest } = require('../')

test('init', t => {
  const ssb = Server()

  t.comment('> check things in the manifest are in the API')
  function walkManifest (manifest, path = []) {
    for (const [key, value] of Object.entries(manifest)) {
      const newPath = [...path, key]
      if (typeof value !== 'string') walkManifest(value, newPath)
      else t.equal(typeof get(ssb.whakapapa, newPath), 'function', `manifest: ${newPath.join('.')}`)
    }
  }
  walkManifest(manifest)

  t.comment('> check things in the API are in the manifest')
  function walkAPI (api, path = []) {
    for (const [key, value] of Object.entries(api)) {
      const newPath = [...path, key]
      if (typeof value !== 'function') walkAPI(value, newPath)
      else t.equal(typeof get(manifest, newPath), 'string', `api: ${newPath.join('.')}`)
    }
  }
  walkAPI(ssb.whakapapa)

  ssb.close()
  t.end()
})
