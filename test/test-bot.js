const Server = require('scuttle-testbot')

module.exports = function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  // }

  const stack = Server

  return stack // eslint-disable-line
    .use(require('ssb-private1'))
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('../')) // ssb-whakapapa
    .call(null, opts)
}
