const CRUT = require('ssb-crut')
const Force = require('ssb-crut/lib/force')

const Link = require('./link')
const View = require('./view')

const spec = require('../spec')
const fixDetails = require('../lib/fix-details')

module.exports = function (ssb) {
  const link = Link(ssb)

  return {
    get (id, cb) {
      link.getLinksOfType(id, 'link/profile-profile/child', cb)
    },
    view: View(ssb),

    child: Methods(spec.link.profileProfile.child),
    partner: Methods(spec.link.profileProfile.partner),

    link
  }

  function Methods (spec) {
    const crut = new CRUT(ssb, spec)
    const force = Force(crut)

    return {
      create (staticProps, props, cb) {
        return crut.create(fixDetails({ ...staticProps, ...props }), cb)
      },
      get (id, cb) {
        return crut.read(id, cb)
      },
      update (id, details, cb) {
        return force.forceUpdate(id, fixDetails(details), cb)
      },
      tombstone (id, details, cb) {
        return force.forceTombstone(id, fixDetails(details), cb)
      },
      list (opts, cb) {
        return crut.list(opts, cb)
      }
    }
  }
}
