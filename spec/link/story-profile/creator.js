module.exports = {
  type: 'link/story-profile/creator',
  staticProps: {
    child: { $ref: '#/definitions/messageId', required: true },
    parent: { $ref: '#/definitions/messageId', required: true }
  },
  props: {}
}
