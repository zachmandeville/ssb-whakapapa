module.exports = {
  link: {
    profileProfile: {
      child: require('./link/profile-profile/child'),
      partner: require('./link/profile-profile/partner')
    },
    storyProfile: {
      contributor: require('./link/story-profile/contributor'),
      creator: require('./link/story-profile/creator'),
      mention: require('./link/story-profile/mention')
    },
    storyArtefact: require('./link/story-artefact'),
    storyStory: require('./link/story-story')
  },
  whakapapapView: require('./whakapapa-view')
}
